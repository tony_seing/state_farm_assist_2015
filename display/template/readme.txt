Show Banner Usage
---------------------

	Change the information in the <h2> to the proper information for the client. Change <h3> to
	have the XXXXX studio number and the job name. Banner size information goes into the <h6> tag.
	Change the revision number, date, and version information as needed.
	
	To update version, copy the HTML block (from START comment to END comment) and use the JavaScript 
	function to point to the SWF file to be loaded. Put all SWF banners in the banners folder. 
	The showBanner() function takes 3 parameters.
	
		showBanner(bannerName:String[, width:Number, height:Number]);
		
	The showBanner() can also take only the first argument if the banner is named in the Studio 8 
	standard naming convention or has the width and height (<width>x<height>) somewhere in the name. 
	The script will be able to evaluate size from the size in the banner string. The standard 
	convention is explained in depth in the Studio 8 wiki. If possible the standard should be used.
	The standard name should be written in this syntax.
	
		<description>_p<number>_<kb>k_<width>x<height>.swf
		
	This script will not work with 1 param if there is no width and height numbers in name. Below are
	examples that will need to use 3 parameters to work. The other a banner may not load is of there is
	a "x" or "X" after the number set. According to the Studio 8 standard convention the banner size 
	should come at the very end of the name before the extension.
	
		23154n_ralphRichMediaLeaveBehind.swf
		21526_p1_clss_mod.swf
		Electrolux_p2_03242008_pausead.swf
		23154c_p4_728x90_weather_xtra.swf
		23154c_p4_728x90_weather_x.swf
	
	These are some examples that will work with 1 param.
	
		repunct_p1_300x250.swf
		celeb_p1a_30k_160x600.swf
		passport_p2a_40k_634x106.swf
		vgs_aces_p1_30k_728x90.swf
		cpck_qn_p2_350x200.swf
		23154c_p4_728x90_weather.swf
		
Show Image Usage
---------------------

	The showImage() function works exactly the same as the showBanner() function above. You can add width
	and height parameters but it is not needed. The method signature is as follows. The first argument is 
	required, the second and third are optional, same as showBanner().
	
		showImage(bannerName:String[, width:Number, height:Number]);
		
Show Site Usage
---------------------

	The showSite() function is used for linking to entire site builds instead using the href attribute. This 
	function should be used because it allows for a frame to be placed above the HTML pages that provides extra
	features. The showSite() function can take 1 to 3 parameters. 
	
		showSite(siteFolder:String[, htmlName:String = 'index.html', useFrame:Boolean = false]);
		
	The first parameter is required and it should be the name of the site build folder. The site build folder 
	should be the same as the version number that appears to the left of the link. This folder name is used in 
	the frame header as the version number. The siteFolder parameter can also be a entire file path for dealing 
	with versions with multiple site links. Below are examples of valid siteFolder parameters.
	
		'001'
		'002/subdir'
		'003/nested/directory'
		
	The second parameter is	optional. It allows for files that are not named as index.html to be linked to.
	The third parameter is optional and it decides whether the page is shown in a frame with a header on top. The
	header provides extra functions and contains the version number of the site being viewed. This parameter is 
	a boolean and can be set to true of false. The default is false.
		
	The showSite() function uses the sitesDirectory variable in the display configuration in the creation 
	of it's link. Only change the variable if a different folder is used to hold the sites.
			
	
Query String Usage
---------------------
	
	To send a specific banner of a single page the same showBanner() params can be passed in a query 
	string. The syntax is below.
	
		http://www.ddb01.com/XXXXX_job/index.html?b=<name>.swf&w=<width>&h=<height>
	
	If the proper naming convention (see above) is followed then only one param needs to be sent 
	in the query string. This is an optional shorthanded approach. If the script cannot evaluate
	the size from the name the page will 'soft error' returning to the versions page.
	
		http://www.ddb01.com/XXXXX_job/index.html?b=<propername>.swf
	
	Images can be sent the same way banners can. Supported image types are PNG, GIF and JPG. Below are
	examples of the query string.
	
		http://www.ddb01.com/XXXXX_job/index.html?b=<image>.gif
		
Versioning Features
---------------------

	There are a number of configuration options that can be used to customize the display of the 
	template. Under the <head> tag there is a <script> tag that holds all the variables that can be 
	changed to customize the template. The comments to the side describe what each variable controls.
	Additionally version control can be overridden with an node level attribute added. To override 
	the versions that show for a single size add a versions attribute to the <h6> tag.
	
		<h6 versions="1">Banner List</h6>
		
	This works the same as the numberOfVersions variable, so if it is set to zero it will show all
	if the attribute does not exist then that grouping will function normally.
	
	There is no need to write version numbers in the <h4> tag if the autoNumbering is set to true.
	This will enumerate the versions automatically, and overwrite any text in the <h4> tag.
			
Sending to the Client
---------------------

	The template has the ability to send a especially configured version to the client by passing in
	a parameter in the query string. This configuration limits version to the most current, removes 
	show versions link and hide revision details. Below is the syntax to follow.
	
		http://www.ddb01.com/XXXXX_job/index.html?t=client
		
	Optionally the client may want to only see a single version and nothing else on the page. The
	template has the ability to send a single through a query string. The parameter name should be 
	single for this to work. Below is the syntax to follow.
	
		http://www.ddb01.com/XXXXX_job/index.html?single=<propername>.swf
		http://www.ddb01.com/XXXXX_job/index.html?single=<bannername>.swf&w=<width>&h=<height>
		
	An image can also be sent the same way. Below is an example.
	
		http://www.ddb01.com/XXXXX_job/index.html?single=<imagename>.gif
		
