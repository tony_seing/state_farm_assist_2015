var bannerPage = false;

function load()
{
	setActiveStyleSheet("default");
	
	if(autoNumbering) autoEnumerate();
	if(autoTableTinting) autoTint();
	
	var params = getParams();
	
	if(params != null && params[0].value.indexOf(".swf") >= 0) { redirectToBanner(); return; }
	
	if(params != null && params[0].value.indexOf(".gif") >= 0) { redirectToImage(); return; }
	if(params != null && params[0].value.indexOf(".png") >= 0) { redirectToImage(); return; }
	if(params != null && params[0].value.indexOf(".jpg") >= 0) { redirectToImage(); return; }
	
	if(params != null && params[0].value == "client")
	{
		numberOfVersions = 1;
		showVersionsLink = false;
		showRevisionList = false;
		showDirectLinks = false;
	}
	
	if(params != null && params[0].name == "frame") addFrame();
	
	if(!showRevisionList) hideRevLists();
	if(showDirectLinks) addMoreSection();
	
	limitVersions();
}

function addFrame()
{
	setActiveStyleSheet("frame");
	
	var qs = location.href.split("?frame=")[1];
	var site = qs.split("&v=")[0];
	var version = location.href.split("&v=")[1];
	
	var back = '<h5><a href="' + location.href.split("?frame=")[0] + '">back</a></h5>';
	var close = '<h5><a href="' + site + '">close frame</a></h5>';
	
	document.body.innerHTML  = '<div id="header"><h4>' + version + '</h4>'+ close + back +'</div>';
	document.body.innerHTML += '<iframe noresize="noresize" src="' + site + '" frameborder="0"></iframe>';
}

function hideRevLists()
{
	var content = document.getElementById("content");
	
	for(i = 0; i < content.childNodes.length; i++)
	{
		if(content.childNodes[i].tagName != undefined)
		{
			if(content.childNodes[i].tagName == "OL")
			{
				content.childNodes[i].style.visibility = "hidden";
			}
		}
	}	
}

function autoTint()
{
	var content = document.getElementById("content");
	
	for(i = 0; i < content.childNodes.length; i++)
	{
		if(content.childNodes[i].tagName == "TABLE")
		{
			var table = content.childNodes[i];
			var rows = table.getElementsByTagName("tr");
			
			for(j = 0; j < rows.length; j++)
			{
				if(j % 2 != 0) rows[j].className = "dark";
				else rows[j].className = "light";
			}
		}
	}
}

function autoEnumerate()
{
	var num = 0;
	var content = document.getElementById("content");
	
	for(i = content.childNodes.length - 1; i >= 0 ; i--)
	{
		if(content.childNodes[i].tagName == "H4" && content.childNodes[i].className != "hide")
		{
			num++;
			content.childNodes[i].innerHTML = num > 99 ? num : (num > 9 ? "0" + num : "00" + num);
		}
		
		if(content.childNodes[i].tagName == "H6") num = 0;
	}
}

function addMoreSection()
{
	var count = 0;
	var content = document.getElementById("content");
	var bannerName = "";
	var bannerNames = [];
	var isSite = false;
	
	for(i = 0; i < content.childNodes.length; i++)
	{
		if(content.childNodes[i].tagName == "H4" && content.childNodes[i].hasChildNodes())
		{
			//get version for frame header
			var version = content.childNodes[i].innerHTML;
		}
		
		//get banner name from show banner link
		if(content.childNodes[i].tagName == "H5" && content.childNodes[i].hasChildNodes())
		{
			if(content.childNodes[i].childNodes[0].tagName == "A")
			{
				//get all params
				var link = String(content.childNodes[i].childNodes[0]);
				var firstPart = link.split("(")[1];
				
				if(firstPart != undefined || firstPart != null)
				{
					var paramString = decodeURI(firstPart.slice(0, firstPart.length - 1));
					var params = paramString.split(",");
					var bannerName = params[0].split("'")[1];
					
					//handle site links
					if(link.split("(")[0] == "javascript:showSite")
					{
						isSite = true;
						bannerName = "";
						
						if(params.length > 1)
						{
							var folder = trim(params[0] , "'");
							var file = trim(params[1], " '");
						}
						
						bannerName = rtrim(location.href, "index.html") + sitesDirectory + "/" + folder + "/" + file;
					}
					else isSite = false;
					
					if(params.length > 1 && !isSite)
					{
						//if additional params are needed
						var width = trim(params[1]).valueOf();
						var height = trim(params[2]).valueOf();
						
						bannerName = bannerName + "&w=" + width + "&h=" + height;
					}
				}
				
				if(bannerName != "") bannerNames.push(bannerName);
			}
		}
		
		if(content.childNodes[i].className == "more")
		{
			var more = content.childNodes[i];
			
			for(j = 0; j < bannerNames.length; j++)
			{
				count++;
				var links = 'javascript:toggleMore(' + count + ')';
				
				if(j == 0)
				{
					var h5 = document.createElement('h5');
					h5.innerHTML = '<a href="' + links + '">' + (isSite ? "Site" : "Banner") + ' Links</a>';
					
					var ul = document.createElement("ul");
					ul.style.display = "none";
					ul.setAttribute('id', String('links' + count));
					
					more.appendChild(h5);
					more.appendChild(ul);
					
					ul.innerHTML = '<li>&nbsp;</li>';
				}
				
				if(!isSite)
				{
					//make banner links
					var singleURL = location.href + '?single=' + bannerNames[j];
					var bannerURL = location.href + '?b=' + bannerNames[j];
					
					sExp = "Send a banner without a back button.";
					bExp = "Send a banner with a back button.";
					
					var name = '<li><b>' + bannerNames[j].split("&")[0] + '</b></li>';
					
					var singleLI = 	'<li>Without Back Button &#8212; <a href="' + singleURL + '" title="' + sExp + '">' 
									+ singleURL + '</a></li>';
					var bannerLI = 	'<li>With Back Button &#8212; <a href="' + bannerURL + '" title="' + bExp + '">' 
									+ bannerURL + '</a></li>';
					
					ul.innerHTML += name;
					ul.innerHTML += singleLI;
					ul.innerHTML += bannerLI;
					ul.innerHTML += '<li>&nbsp;</li>';
				}
				else
				{
					//make site links
					var singleURL = bannerNames[j];
					var bannerURL = location.href + "?frame=" + bannerNames[j] + "&v=" + version;
					
					sExp = "Send the site without the header.";
					bExp = "Send the site with the header.";
					
					var name = '<li><b>' + file + '</b></li>';
					
					var singleLI = 	'<li>Without Header &#8212; <a href="' + singleURL + '" title="' + sExp + '">' 
									+ singleURL + '</a></li>';
					var bannerLI = 	'<li>With Header &#8212; <a href="' + bannerURL + '" title="' + bExp + '">'
									+ bannerURL + '</a></li>';
					
					ul.innerHTML += name;
					ul.innerHTML += singleLI;
					ul.innerHTML += bannerLI;
					ul.innerHTML += '<li>&nbsp;</li>';
				}
			}
			
			bannerNames = [];
		}
	}
}

function toggleMore(i)
{
	var links = document.getElementById("links" + i);
	
	if(links.style.display == "none") links.style.display = "inline";
	else links.style.display = "none";
}

function showSite(num, file, frame)
{
	if(file == null) file = "index.html";
	
	var site = './' + sitesDirectory + '/' + num + '/' + file;	
	var version = num.split("/")[0];
	
	var back = '<h5><a href="' + location.href + '">back</a></h5>';
	var close = '<h5><a href="' + site + '">close frame</a></h5>';
	
	if(frame == false || location.href.indexOf("?t=client") != -1)
	{
		location.href = site;
	}
	else if(frame == null || frame)
	{
		setActiveStyleSheet("frame");
		document.body.innerHTML  = '<div id="header"><h4>' + version + '</h4>'+ close + back +'</div>';
		document.body.innerHTML += '<iframe noresize="noresize" src="' + site + '" frameborder="0"></iframe>';
	}
}

function limitVersions()
{
	var numVers = numberOfVersions + 1;
	var vers = 0;
	var content = document.getElementById('content');
	var titleCount = 0;
	
	for(i = 0; i < content.childNodes.length; i++)
	{
		if(content.childNodes[i].tagName != undefined)
		{
			//if versions attribute has been set do numVers exception
			if(content.childNodes[i].getAttribute("versions") && content.childNodes[i].tagName == "H6")
			{
				numVers = Number(content.childNodes[i].getAttribute("versions")) + 1;
			}
			else if(!content.childNodes[i].getAttribute("versions") && content.childNodes[i].tagName == "H6")
			{
				numVers = numberOfVersions + 1;
			}
		
			if(numVers != 1)
			{
				if(content.childNodes[i].tagName == "H4") vers++;
				if(vers >= numVers && content.childNodes[i].tagName != "H6") content.childNodes[i].style.display = "none";
				
				if(content.childNodes[i].tagName == "H6")
				{
					titleCount++;
					
					var revEnd = content.childNodes[i];
										
					if(showVersionsLink)
					{
						if(!bannerPage)
						revEnd.nextSibling.innerHTML = '<a href="javascript:showVersions(' + titleCount + ')">show versions</a>';
					}
				}
				
				//if versions is equal or more than remove version link
				if(revEnd != undefined && showVersionsLink)
				{
					vers >= numVers 
						? revEnd.nextSibling.innerHTML = '<a onclick="showVersions(' + titleCount + ')">show versions</a>' 
						: revEnd.nextSibling.innerHTML = "";
				}
					
				if(content.childNodes[i].tagName == "H6") vers = 0;
			}
		}
	}
}

function showVersions(titleCount)
{
	var count = 0;
	var content = document.getElementById('content');
	
	for(i = 0; i < content.childNodes.length; i++)
	{
		if(content.childNodes[i].tagName != undefined)
		{
			if(content.childNodes[i].tagName == "H6") count++;
			if(count == titleCount) content.childNodes[i].style.display = "block";			
			
			if(content.childNodes[i].tagName == "H6" && count == titleCount)
			{
				var title = content.childNodes[i];
				title.nextSibling.innerHTML = '<a onclick="hideVersions(' + count + ')">hide versions</a>';
			}
		}
	}
}

function hideVersions(titleCount)
{
	var numVers = numberOfVersions + 1;
	var vers = 0;
	
	var count = 0;
	var content = document.getElementById('content');
	
	for(i = 0; i < content.childNodes.length; i++)
	{
		if(content.childNodes[i].tagName != undefined)
		{
			//if versions attribute has been set do numVers exception
			if(content.childNodes[i].getAttribute("versions") && content.childNodes[i].tagName == "H6")
			{
				numVers = Number(content.childNodes[i].getAttribute("versions")) + 1;
			}
			else if(!content.childNodes[i].getAttribute("versions") && content.childNodes[i].tagName == "H6")
			{
				numVers = numberOfVersions + 1;
			}
			
			if(content.childNodes[i].tagName == "H4") vers++;
			if(content.childNodes[i].tagName == "H6") count++;
			
			if(vers >= numVers && content.childNodes[i].tagName != "H6" && count == titleCount)
				content.childNodes[i].style.display = "none";
			
			if(content.childNodes[i].tagName == "H6" && count == titleCount)
			{
				var title = content.childNodes[i];
				title.nextSibling.innerHTML = '<a onclick="showVersions(' + count + ')">show versions</a>';
			}
			
			if(content.childNodes[i].tagName == "H6") vers = 0;
		}
	}
}

function showImage(imageName, width, height)
{
	if(!width) width = 0; if(!height) height = 0;
	showBanner(imageName, width, height, true);
}

function showBanner(bannerName, width, height, image)
{
	bannerPage = true;
	
	//attempt to get size from banner name
	if(width == null || height == null)
	{
		var name = bannerName.split(".");
		var parts = name[0].split("_");
		
		//look for size
		for(i = parts.length - 1; i >= 0; i--)
		{
			if(parts[i].indexOf("x") != -1) { nums = parts[i]; break; }
		}
		
		//get width and height
		var num = nums.split(new RegExp("x", "i")); 
		
		//assign and handle failures
		width = num[0]; height = num[1];
		if(height == null || isNaN(height)) location.href = "index.html";
	}
	
	var content = document.getElementById('content');
	
	//if client return to client index changes return links
	var url = location.href.split("/");
	url[url.length - 1].indexOf("client") != -1 ? isClient = true : isClient = false;
	
	showBackLink 
		? backLink = 	'<a onclick="location.href = \'index.html' 
						+ (isClient ? "?t=client" : "") + '\'">back</a>' 
		: backLink = 	"";
	
	//hide back if single
	var params = getParams();	
	if(params != null && params[0].name == "single") backLink = ""; 	
		
	var adobeImage = '<img src="template/getfla.gif" alt="Get Flash" border="0" />';
	var adobeLink = '<a href="http://www.adobe.com/go/getflashplayer">'+ adobeImage +'</a>';
	var adobeText = '<p>This requires Flash Player 8 or above.</p>';
	
	params != null && params[0].name == "single" ? bName = "single" : bName = "b";
	
	var queryString = '?' + bName + '=' + bannerName + '&w=' + width + '&h=' + height;
	showReplayLink 
		? replayLink = 	'<a onclick="location.href = \'index.html' + queryString 
						+ (isClient ? "&t=client" : "") + '\'">replay</a>' 
		: replayLink = "";
	
	//show the banner html
	content.innerHTML  = 	'<h6>' + bannerName + 
							(!isIE() ? ('<h5>' + (!image ? replayLink : "") + backLink + '</h5>') : "") + '</h6>';
	content.innerHTML += 	isIE() ? '<h5>' + (!image ? replayLink : "") + backLink + '</h5>' : "";
	content.innerHTML += 	'<h4 class="hide">&nbsp;</h4>';
	content.innerHTML += 	'<h5>&nbsp;</h5>';
	content.innerHTML += 	'<div id="banner">' + adobeLink + adobeText + '</div>';
	
	//load swf
	if(!image) swfobject.embedSWF(bannerDirectory + '/' + bannerName, 'banner', String(width), String(height), '8.0.0');
	
	//load image
	if(image)
	{
		document.getElementById("banner").innerHTML = 
		'<img src="' + imageDirectory + '/' + bannerName + '" border="0" ' +
		(width != 0 || height != 0 ? 'width="' + width + '" height="' + height + '"' : "") + '/>';
	}
}

function redirectToBanner()
{
	var params = getParams(); 
	if(params == null) return;
	
	var banner = params[0].value;
	
	if(params.length > 1)
	{
		var width = params[1].value;
		var height = params[2].value;
		
		showBanner(banner, width, height);
	}
	else
	{
		showBanner(banner);
	}
}

function redirectToImage()
{
	var params = getParams(); 
	if(params == null) return;
	
	var banner = params[0].value;
	
	if(params.length > 1)
	{
		var width = params[1].value;
		var height = params[2].value;
		
		showImage(banner, width, height);
	}
	else
	{
		showImage(banner);
	}
}

function getParams()
{
    var queryParams = window.location.search.substring(1);
    if(queryParams == "") return null;

    var resultArray = new Array();
    var qpSplit = new RegExp('[&=]');
    resultArray = queryParams.split(qpSplit);
    
    var pairArray = new Array();
    var resultObject = new Object();
    
    for(var i = 0; i < resultArray.length; i++)
    {
    	i % 2 == 0 ? resultObject.name = resultArray[i] : resultObject.value = resultArray[i];
    	i % 2 == 0 ? pairArray.push(resultObject) : resultObject = new Object();
    }
    
    return pairArray;
}

function isIE()
{
	if(/MSIE (\d+\.\d+);/.test(navigator.userAgent)) return true;
	else return false;
}

function trim(str, chars)
{
	return ltrim(rtrim(str, chars), chars);
}
 
function ltrim(str, chars)
{
	chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
 
function rtrim(str, chars)
{
	chars = chars || "\\s";
	return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

function setActiveStyleSheet(title)
{
	var i, a, main;
	
	for(i = 0; (a = document.getElementsByTagName("link")[i]); i++) 
	{
		if(a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title"))
		{
			a.disabled = true;
			if(a.getAttribute("title") == title) a.disabled = false;
		}
	}
}



